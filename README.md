# *** BarCodeReader *** #


Scan the barcode from the picture by putting its name in this simple C++ console application and get the decoded digits!


This project was implemented for the university lecture Vision Systems,
It presents the way to read the encoded digits from the set of bars in the raw picture.

The flow of the program is presented below:
![sequenceDiagram](resources/ProcessDiagramUML.JPG)
