//=================================================================================================
// File:    <name_of_file>
// Created: <DD-MM-YYYY, HH:MM>
//
// <Description>
//=================================================================================================
#pragma once

//===================================== Included =============================================
#include <iostream>
#include <string>






//============================== Constants and definitions ===================================

// Maximum and constant number of bars in the digit
#define BARS_IN_DIGIT 7





//================================== Types and globals =======================================

typedef enum
{
  Odd,
  Even
} DigitParity;

// Two different types of encoding for digits
typedef enum DigitsHandEncodingTypes
{
  leftHand,
  rigthHand,
  maxHand
} DigitsEncoding;






//===================================== Classess =============================================
class DigitEncodingTable
{
private:
  //==================================== Variables ========================================
  
  // Structure with each digit data
  typedef struct DigitDataStructure
  {
    bool barsValues[BARS_IN_DIGIT];
    std::string decodedDigit;
    DigitParity parity;
  } DigitDatas;
  
  
  
  
  //=============================== Functions and methods =================================

  //=============================================================================
  // Function:  DecodeRightHand    - Decodes the digit from right hand encoding
  // Parameter: barsValues         - Array with the boolean bar values
  // Returns:   Single character decoded from bars
  //=============================================================================
  std::string DecodeRightHand( bool barsValues[ BARS_IN_DIGIT ] );

  //=============================================================================
  // Function:  DecodeLeftHand     - Decodes the digit from left hand encoding
  // Parameter: barsValues         - Array with the boolean bar values
  // Returns:   Single character decoded from bars
  //=============================================================================
  std::string DecodeLeftHand( bool barsValues[ BARS_IN_DIGIT ], DigitParity *parities );



public:
  //==================================== Variables ========================================




  //=============================== Functions and methods =================================

  //=============================================================================
  // Function:  DecodeFirstDigit   - Decodes the first digit by gathered parities
  // Parameter: None
  // Returns:   One character string with the first digit of barcode
  //=============================================================================
  char DecodeFirstDigit( DigitParity *parities );


  //=============================================================================
  // Function:  DigitDecodingTable - Default constructor
  // Parameter: None
  // Returns:   None
  //=============================================================================
  DigitEncodingTable( );

  //=============================================================================
  // Function:  DigitDecodingTable - Default destructor
  // Parameter: None
  // Returns:   None
  //=============================================================================
  ~DigitEncodingTable( );


  //=============================================================================
  // Function:  DecodeDigitChar    - Decodes the digit from encoded character
  // Parameter: barsValues         - Array with the boolean bar values
  //            handEncoding       - Left or right hand indicator for encoding
  // Returns:   Single character decoded from bars
  //=============================================================================
  std::string DecodeDigitChar( bool barsValues[], DigitsEncoding handEncoding, DigitParity *parities );

};







//================================= Functions definitions ====================================


