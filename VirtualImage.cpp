//=================================================================================================
// File:    VirtualImage.cpp
// Created: 20-12-2016, 18:27
//
// These are the definitions of the image mapped on the virtual space. Here are contained all the
// methods used for working with the image.
//=================================================================================================

//===================================== Included =============================================
#include "VirtualImage.h"





//============================== Constants and definitions ===================================

//=============================================================================
// Function:  MeasureCodeLine  - Checks the lenght of the line from barcode
// Parameter: virtualImage     - Whole image with the barcode
//            linePosition     - number of column with the measured line
//            beginning        - Number of row where line begins
// Returns:   Length of the measured line
//=============================================================================
static int MeasureCodeLine( const bool **virtualImage, const int linePosition, const int beginning );








//================================== Types and globals =======================================







//================================= Functions definitions ====================================

//=============================================================================
VirtualImage::VirtualImage( bool **pixels, int imageSizeX, int imageSizeY )
{
  // Clarify the input and store it
  if( nullptr != pixels && 0 != imageSizeX && 0 != imageSizeY )
  {
    std::cout << "#  Virtual image created with size: " << imageSizeX << "x" << imageSizeY << std::endl;
    std::cout << "#  Verifying... ";
    // Store the image size in the own storage
    virtualImageSize[ sizeX ] = imageSizeX;
    virtualImageSize[ sizeY ] = imageSizeY;

    // Verify the image by checking the allocation
    int columnCounter = 0;
    for( columnCounter = 0; columnCounter < imageSizeY; ++columnCounter )
    {
      if( nullptr == pixels[ columnCounter ] )
      {
        std::cout << "#  Bad virtual image allocation in the row: " << columnCounter << std::endl;
        break;
      }
    }
  }
  else
  {
    std::cout << "#  Virtual image not created successfuly!" << std::endl;
  }
  std::cout << "\n#  Verified!" << std::endl;
}
//=============================================================================






//=============================================================================
VirtualImage::~VirtualImage( )
{
}
//=============================================================================






//=============================================================================
bool VirtualImage::SearchForPatterns( const bool **virtualImage )
{
  // Store the status of finding
  bool patternsFound = false;
  int previousLine = 0;

  // constant amount of patterns in the barcode
  const int amountPatterns = 3;

  int RowCounter = 0, columnCounter = 0;

  // count the patterns already found
  int currentPatterns = 0;
  while( columnCounter < virtualImageSize[ sizeX ] )
  {
    // Find the first line and follow
    if( true == virtualImage[ RowCounter ][ columnCounter ] )
    {
      int lineLength = MeasureCodeLine( virtualImage, columnCounter, RowCounter );
      if( lineLength > previousLine )
      {
        guardianLength = lineLength;
        // Store the position of each pattern in the barcode
        if( middleGuard == currentPatterns )
        {
          // Middle one starts with zero, so move the position by one bar to the left
          guardiansPosition[ currentPatterns ] = columnCounter - barWidth;
          currentPatterns++;
        }
        else
        {
          guardiansPosition[ currentPatterns ] = columnCounter;
          currentPatterns++;
        }
        std::cout << "#  Found pattern in the bar: " << guardiansPosition[ currentPatterns-1 ] << std::endl;
        if( currentPatterns == 1 )
        {
          std::cout << "#  First guardian was found - reading bar width" << std::endl;
          // This is the first pattern - start sentinel
          GetResolution( virtualImage[ RowCounter ], columnCounter );
          std::cout << "#  Virtual image resolution (bar width) is: " << barWidth << std::endl;
        }
      }
      previousLine = lineLength;
    }

    // Move by one column to the right
    columnCounter++;
  }

  // Check if minimum number of patterns was found
  if( amountPatterns == currentPatterns )
  {
    patternsFound = true;
  }
  else
  {
    std::cout << "#  Pattern couldn't be found!" << std::endl;
  }

  // Notify about the success
  return patternsFound;
}
//=============================================================================






//=============================================================================
bool VirtualImage::DecodeBarcode( bool **virtualImage )
{
  std::cout << "\n#  Finding patterns" << std::endl;

  if( true == SearchForPatterns( const_cast<const bool**>(virtualImage) ) )
  {
    // Patterns found and barcode recognized - gather the digits and decode
    int firstDigitColumn = guardiansPosition[ startSentinel ] + borderSentinel*barWidth;

    std::cout << "\n#  Starting to read the digits from bar: " << firstDigitColumn << std::endl;
    // Keep the number of digits in the barcode, so the status can be verified constantly
    typedef enum
    {
      leftHandAmount = 6,
      rightHandAmount = 6
    } HandDigitsSideAmount;
    // Leave the spot for the first digit
    finalCode += '0';
    // Read the left-hand digits
    int digitsCount = 0;
    int safeRow = SecureSkew( virtualImage );
    int currentBar = firstDigitColumn + digitsCount*BARS_IN_DIGIT;
    for( digitsCount = 0; digitsCount < leftHandAmount; ++digitsCount )
    {
      finalCode += DecodeSign( virtualImage[ safeRow ], currentBar, leftHand );
      currentBar += BARS_IN_DIGIT*barWidth;
    }
    // Pass the center guard
    currentBar += centerGuard*barWidth;
    std::cout << "#  Center guard spotted - reading the right-hand digits" << std::endl;

    for( digitsCount = 0; digitsCount < rightHandAmount; ++digitsCount )
    {
      finalCode += DecodeSign( virtualImage[ safeRow ], currentBar, rigthHand );
      currentBar += BARS_IN_DIGIT*barWidth;
    }
    std::cout << "#  Reading ended successfully!" << std::endl;
  }

  DigitEncodingTable DigitTable;
  std::cout << "#  Decoding the first digit..." << std::endl;
  finalCode[ 0 ] =  DigitTable.DecodeFirstDigit( parities );

  return true;
}
//=============================================================================






//=============================================================================
int VirtualImage::SecureSkew( bool **virtualImage )
{
  // Store the finding
  int safeRow = 0;

  std::cout << "#  Securing the skew" << std::endl;
  // Prepare the scan of the image
  int column, row = 0;

  for( row = 0; row < virtualImageSize[sizeY]; ++row )
  {
    int distance = 0;
    int lastDistance = distance;
    for( column = 0; column < virtualImageSize[sizeX]; ++column )
    {
      if( true == virtualImage[ row ][ column ] )
      {
        // Firts color found - compare the distance
        column++;
        while( true != virtualImage[ row ][ column ] )
          column++; distance++;
        break;
      }
    }
    if( lastDistance != distance )
    {
      std::cout << "#  Skew found at row: " << row << std::endl;
      break;
    }
  }

  // Make sure that any skew was found
  if( guardianLength < row )
  {
    safeRow = guardianLength / 2;
    std::cout << "#  Safest row is: " << safeRow << std::endl;
  }
  // Notify about the skew finding
  return safeRow;
}
//=============================================================================






//=============================================================================
int MeasureCodeLine( const bool **virtualImage, const int linePosition, const int beginning )
{
  int lineLength = beginning;
  while( virtualImage[ lineLength ][ linePosition ] )
  {
    lineLength++;
  }

  // Align the counted length
  lineLength -= beginning;

  return lineLength;
}
//=============================================================================






//=============================================================================
void VirtualImage::GetResolution( const bool *barcodeRow, const int linePosition )
{
  // Prepare the width of measured bar in the start sentinel
  int barLineWidth = linePosition;

  // Move forward through the bar and check it
  while( true == barcodeRow[ barLineWidth ] )
  {
    ++barLineWidth;
  }

  if( 0 == barLineWidth )
  {
    std::cout << "Error in measuring the barcode resolution!" << std::endl;
  }
  else
  {
    barWidth = barLineWidth - linePosition;
  }
}
//=============================================================================






//=============================================================================
std::string VirtualImage::DecodeSign( bool *barcodeLine, int startingBar, DigitsEncoding handSide )
{
  // Deduct the size of guardians in the barcode
  const int guardianSize[ maximum ] = {
    borderSentinel*barWidth,
    centerGuard*barWidth,
    borderSentinel*barWidth
  };

  std::cout << "   Reading bars: ";
  // Get the next digit by reading the bars
  bool digitBars[ BARS_IN_DIGIT ] = { 0 };
  int barCount = startingBar;

  for( barCount = 0; barCount < BARS_IN_DIGIT; barCount++ )
  {
    digitBars[ barCount ] = barcodeLine[ startingBar + barCount*barWidth ];
    //if( barcodeLine[startingBar+barCount*barWidth ] )
    std::cout << digitBars[ barCount ];
  }

  std::cout << std::endl;

  // Get the decoded character with digit
  DigitEncodingTable DigitTable;
  std::string decodedCharacter = DigitTable.DecodeDigitChar( digitBars, handSide, parities );

  return decodedCharacter;
}
//=============================================================================






//=============================================================================
void VirtualImage::DisplayDigits( void )
{
  std::cout << "\n\n#  Decoded digits are:  " << std::endl;
  std::cout << "#                 ---- " << finalCode << " ----" << std::endl;
}
//=============================================================================

