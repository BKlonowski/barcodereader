//=================================================================================================
// File:    VirtualImage.h
// Created: 20-12-2016, 18:26
//
// This is the class declaration which consist the abstract existance of virtual image mapped on the
// dedicated space. Here can be found the methods declarations related to handling, reading and 
// analysing the virtual image.
//=================================================================================================
#pragma once

//===================================== Included =============================================
#include "DigitEncodingTable.h"
#include <math.h>





//============================== Constants and definitions ===================================

// Defines the maximum number of picture dimensions
#define MAX_PIC_DIMENSIONS 2






//================================== Types and globals =======================================

// Position of each barcode size
typedef enum BarcodeImageSizeIndicators
{
  sizeX = 0,
  sizeY = 1,
  maxSize
} BarcodeSize;


// Types of sentinels in the barcode
typedef enum BarcodeSentinelTypes
{
  startSentinel,
  middleGuard,
  endSentinel,
  
  maximum = 3
} Sentinels;








//===================================== Classess =============================================
class VirtualImage
{
private:
  //==================================== Variables ========================================

  // Stores the barcode decoded from picture
  std::string finalCode;

  // Store the size of the created virtual image. index 0 is X, 1 is Y
  int virtualImageSize[ maxSize ];

  // Width of one bar of barcode
  int barWidth;

  // Remember the place with beginning of the guardians
  int guardiansPosition[ maximum ];
  int guardianLength;

  DigitParity parities[ 6 ];



  //=============================== Functions and methods =================================

  //=============================================================================
  // Function:  SearchForPatterns - Finds the most important elements of barcode
  // Parameter: virtualImage
  // Returns:   Boolean status of finding. True if found
  //=============================================================================
  bool SearchForPatterns( const bool **virtualImage );


  //=============================================================================
  // Function:  SecureSkew   - Checks if there are skews in the image
  // Parameter: virtualImage
  // Returns:   Integer value of the safe row which will be read
  //=============================================================================
  int SecureSkew( bool **virtualImage );


  //=============================================================================
  // Function:  IsProperBarcode - Ensures the image contains the barcode
  // Parameter: virtualImage
  // Returns:   Boolean value indicating proper barcode picture.
  //=============================================================================
  bool IsProperBarcode( bool **virtualImage );


  //=============================================================================
  // Function:  GetResolution   - Finds the resolution of code via the significant
  // Parameter: virtualImageRow - Single row with ordered bars (it is enough)
  //            linePosition    - number of column with the start sentinel spot
  // Returns:   None
  //=============================================================================
  void GetResolution( const bool *virtualImageRow, const int linePosition );


  //=============================================================================
  // Function:  DecodeSign      - Decodes one digit of the barcode (left to right)
  // Parameter: barcodeLine     - One segment of barcode
  //            startingBar     - The number of each bar from reading should start
  //            handSide        - The specific enumerated side of the barcode
  // Returns:   One barcode digit
  //=============================================================================
  std::string DecodeSign( bool *barcodeLine, const int startingBar, DigitsEncoding handSide );





public:
  //==================================== Variables ========================================

  // Amount of bars in the each sentinel
  typedef enum
  {
    borderSentinel = 3,
    centerGuard    = 5
  } SentinelSize;




  //=============================== Functions and methods =================================
  
  //=============================================================================
  // Function:  DecodeBarcode - Decodes the barcode given as image input
  // Parameter: None
  // Returns:   Boolean status of decoding. True if succedded
  //=============================================================================
  bool DecodeBarcode( bool **virtualImage );


  //=============================================================================
  // Function:  VirtualImage - overloaded constructor of image.
  // Parameter: pixels       - set of pixels given as two-dimensional array
  //            imageSize    - dimensions set of picture. X/Y
  // Returns:   None
  //=============================================================================
  VirtualImage( bool **pixels, int imageSizeX, int imageSizeY );



  //=============================================================================
  // Function:  VirtualImage - Default destructor of virtual image.
  // Parameter: None
  // Returns:   None
  //=============================================================================
  ~VirtualImage();


  //=============================================================================
  // Function:  DisplayDigits - Displays the decoded barcode digits
  // Parameter: None
  // Returns:   None
  //=============================================================================
  void DisplayDigits( void );
};







