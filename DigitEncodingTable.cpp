//=================================================================================================
// File:    <name_of_file>
// Created: <DD-MM-YYYY, HH:MM>
//
// <Description>
//=================================================================================================

//===================================== Included =============================================
#include "DigitEncodingTable.h"






//============================== Constants and definitions ===================================







//================================== Types and globals =======================================







//================================= Functions definitions ====================================

//=============================================================================
DigitEncodingTable::DigitEncodingTable()
{
}
//=============================================================================




//=============================================================================
DigitEncodingTable::~DigitEncodingTable()
{
}
//=============================================================================





//=============================================================================
std::string DigitEncodingTable::DecodeDigitChar( bool barsValues[], DigitsEncoding handEncoding,DigitParity *parities )
{
  // Store the single decoded character
  std::string decodedDigit = "0";

  // Find the gathered bars in the proper table
  switch( handEncoding )
  {
  case leftHand:
    decodedDigit = DecodeLeftHand( barsValues, parities );
    break;
  case rigthHand:
    decodedDigit = DecodeRightHand( barsValues );
    break;
  default:
    std::cout << "#  Wrong hand side encoding!" << std::endl;
    break;
  }

  // Give the decoded digit
  return decodedDigit;
}
//=============================================================================






//=============================================================================
std::string DigitEncodingTable::DecodeLeftHand( bool barsValues[ BARS_IN_DIGIT ], DigitParity *parities )
{
  // Keep in mind the current parity
  static int parityCount = 0;

  // Prepare the storage for decoded character
  std::string decodedDigit = "0";

  // Left hand encoding table for comparison
  DigitDatas LeftHandDigits[] =
  {
    { { false, false, false, true, true, false, true }, "0", Odd },
    { { false, true, false, false, true, true, true }, "0", Even },
    { { false, false, true, true, false, false, true }, "1", Odd },
    { { false, true, true, false, false, true, true }, "1", Even },
    { { false, false, true, false, false, true, true }, "2", Odd },
    { { false, false, true, true, false, true, true }, "2", Even },
    { { false, true, true, true, true, false, true }, "3", Odd },
    { { false, true, false, false, false, false, true }, "3", Even },
    { { false, true, false, false, false, true, true }, "4", Odd },
    { { false, false, true, true, true, false, true }, "4", Even },
    { { false, true, true, false, false, false, true }, "5", Odd },
    { { false, true, true, true, false, false, true }, "5", Even },
    { { false, true, false, true, true, true, true }, "6", Odd },
    { { false, false, false, false, true, false, true }, "6", Even },
    { { false, true, true, true, false, true, true }, "7", Odd },
    { { false, false, true, false, false, false, true }, "7", Even },
    { { false, true, true, false, true, true, true }, "8", Odd },
    { { false, false, false, true, false, false, true }, "8", Even },
    { { false, false, false, true, false, true, true }, "9", Odd },
    { { false, false, true, false, true, true, true }, "9", Even }
  };

  // Parse through the encoding table and compare with given bars
  int tableEntryCount = 0;
  for( tableEntryCount = 0; tableEntryCount < 20; ++tableEntryCount )
  {
    // Compare the bars with the table
    int barCount = BARS_IN_DIGIT;
    do
    {
      barCount--;
      if( LeftHandDigits[ tableEntryCount ].barsValues[ barCount ] != barsValues[ barCount ] )
      {
        // Encoded bars mismatch - stop this search
        break;
      }
    } while( barCount );
    if( 0 == barCount )
    {
      // Every bars match! Digit was found!
      decodedDigit = LeftHandDigits[ tableEntryCount ].decodedDigit;
      parities[ parityCount ] = LeftHandDigits[ tableEntryCount ].parity;
      parityCount++;

      // W jakiej� funkcji por�wnaj kolejno�� parzystych i nieparzystych
      // Wyci�gnij z tego pierwszy znak  (firstDigit)  i przechowaj w klasie (prywatnie!!)
      // Oddawaj przechowany first digit w funkcji  publicznej!!
      break;
    }
  }

  return decodedDigit;
}
//=============================================================================






//=============================================================================
std::string DigitEncodingTable::DecodeRightHand( bool barsValues[ BARS_IN_DIGIT ] )
{
  // Prepare the storage for decoded character
  std::string decodedDigit = "0";

  DigitDatas rightHandDigits[ ] =
  {
    { { true, true, true, false, false, true, false }, "0" },
    { { true, true, false, false, true, true, false }, "1" },
    { { true, true, false, true, true, false, false }, "2" },
    { { true, false, false, false, false, true, false }, "3" },
    { { true, false, true, true, true, false, false }, "4" },
    { { true, false, false, true, true, true, false }, "5" },
    { { true, false, true, false, false, false, false }, "6" },
    { { true, false, false, false, true, false, false }, "7" },
    { { true, false, false, true, false, false, false }, "8" },
    { { true, true, true, false, true, false, false }, "9" }
  };

  // Parse through the encoding table and compare with given bars
  int tableEntryCount = 0;
  for( tableEntryCount = 0; tableEntryCount < 20; ++tableEntryCount )
  {
    // Compare the bars with the table
    int barCount = BARS_IN_DIGIT;
    do
    {
      barCount--;
      if( rightHandDigits[ tableEntryCount ].barsValues[ barCount ] != barsValues[ barCount ] )
      {
        // Encoded bars mismatch - stop this search
        break;
      }
    } while( barCount );
    if( 0 == barCount )
    {
      // Every bars match! Digit was found!
      decodedDigit = rightHandDigits[ tableEntryCount ].decodedDigit;
      break;
    }
  }

  return decodedDigit;
}
//=============================================================================





//=============================================================================
char DigitEncodingTable::DecodeFirstDigit( DigitParity *parities )
{
  struct FirstDigitEncoding
  {
    DigitParity parities[ 6 ];
    char firstDigit;
  };

  // Create the comparison table
  FirstDigitEncoding comparisonTable[] =
  {
    { { Odd, 	Odd, 	Odd,	Odd, 	Odd, 	Odd }, '0' },
    { { Odd, Odd, Even, Odd, Even, Even }, '1' },
    { { Odd, 	Odd, 	Even, 	Even, 	Odd, 	Even }, '2' },
    { { Odd,	Odd, 	Even,	Even, 	Even, 	Odd }, '3' },
    { { Odd,	Even, 	Odd, 	Odd, Even, Even }, '4' },
    { { Odd, Even, Even, Odd, Odd, Even }, '5' },
    { { Odd, Even, Even, Even, Odd, Odd }, '6' },
    { { Odd, Even, Odd, Even, Odd, Even }, '7' },
    { { Odd, Even, Odd, Even, Even, Odd }, '8' },
    { { Odd, Even, Even, Odd, Even, Odd }, '9' }
  };

  int tableEntryCount = 0;
  for( tableEntryCount = 0; tableEntryCount < 10; ++tableEntryCount )
  {
    // Compare the bars with the table
    int barCount = 6;
    do
    {
      barCount--;
      if( comparisonTable[ tableEntryCount ].parities[ barCount ] != parities[ barCount ] )
      {
        // Encoded bars mismatch - stop this search
        break;
      }
    } while( barCount );

    if( 0 == barCount )
    {
      break;
    }
  }
  // When found the decoded digit return it
  return comparisonTable[ tableEntryCount ].firstDigit;
}
//=============================================================================
