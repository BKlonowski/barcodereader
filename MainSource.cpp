//=================================================================================================
// File:    MainSource.cpp
// Created: 10-12-2016, 14:42
//
// The main file of the whole project. Here is included only the main function flowing the barcode
// reader functionallity. Please remember to keep this file as short and small as possible.
//=================================================================================================

//===================================== Included =============================================
#include "FileHandling.h"
#include "VirtualImage.h"



//===================================== Classess =============================================




//============================== Constants and definitions ===================================





//================================== Types and globals =======================================




//================================= Functions definitions ====================================

//=============================================================================
// Function:  main    - This is the main function of the system.
// Parameter: None
// Returns:   EXIT status related to the Windows OS
//=============================================================================
int main( void )
{
  // Start the program - ask for picture file name to decode
  std::cout << "Give the name of the image file to open: ";
  char userName[ 20 ] = { '\0' };
  std::cin >> userName;
  std::cout << std::endl;

  // Handle the picture file
  FileHandling FileHand( userName );

  // Prepare the place for the map basing on the previous parsing
  bool **image = new bool*[ FileHand.giveSize( 'Y' ) ];
  int columnCounter = 0;
  for( columnCounter = 0; columnCounter < FileHand.giveSize( 'Y' ); ++columnCounter )
  {
    image[ columnCounter ] = new bool[ FileHand.giveSize( 'X' ) ];
  }

  if( true == FileHand.Virtualize( image ) )
  {
    // Create the full virtual image
    VirtualImage Image( image, FileHand.giveSize( 'X' ), FileHand.giveSize( 'Y' ) );
    Image.DecodeBarcode( image );
    int column, row;
    for( row = 0; row < FileHand.giveSize( 'Y' ); ++row )
    {
      for( column = 0; column < FileHand.giveSize( 'X' ); ++column )
      {
        std::cout << image[ row ][ column ];
      }
      std::cout << std::endl;
    }
    Image.DisplayDigits();
  }


  // Delete the image - it is not neccessarily and memory is secured
  std::cout << "\n#  Deleting the virtual image..." << std::endl;
  int column = 50;
  do
  {
    column--;
    delete[ ] image[ column ];
  } while( column );

  // Successfully deleted the image - get rid of main pointer
  delete[]image;
  
  // Securly end the program
  system( "PAUSE" );
  return EXIT_SUCCESS;
}
