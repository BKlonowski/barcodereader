//=================================================================================================
// File:    FileHandling.cpp
// Created: 0-12-2016, 14:37
//
// Definitions of every methods and functions declared in the header file. These are the mechanisms
// contributors handling the image file cases.
//=================================================================================================

//===================================== Included =============================================
#include "FileHandling.h"



//===================================== Classess =============================================




//================================== Types and globals =======================================

// Defines the standard size of integer value in BMP
#define STD_INT_SIZE 4

// Bits amount in the byte
#define BITS_IN_BYTE 8

// Standard size of low value in BMP
#define STD_LOW_SIZE 2



// Types of DIB considered
typedef enum
{
  NoDIB,    // Indicates the error in reading
  Info,     // BITMAPINFOHEADER
  Core      // BITMAPCOREHEADER

} DIBKinds;




//============================== Constants and definitions ===================================

//=============================================================================
// Function:  EnsureBMPfile   - Indicates whether proper BMP file was opened
// Parameter: image           - pointer to the opened image file.
// Returns:   Boolean value indicating BMP finding. TRUE if BMP spotted
//=============================================================================
static bool EnsureBMPFile( std::fstream *imageFile );




//=============================================================================
// Function:  DeductDIBHeader - Indicates which DIB header is being read and by
//                              this information deduct the proper size of moving
// Parameter: imageFile       - pointer to the opened image file.
// Returns:   size of the information fields from the DIB header
//=============================================================================
static int GetProperInfoSize( std::fstream *imageFile );




//=============================================================================
// Function:  GetValueOfInfo  - Gets the value of the next info included in header
//                              This function starts reading from the next file position
// Parameter: imageFile       - pointer to the opened image file.
//            bitPasser       - Indicates the size of information to read
// Returns:   value of the information contained the DIB header
//=============================================================================
static int GetValueOfInfo( int bitPasser, std::fstream *imageFile );




//=============================================================================
// Function:  GetPixelValue   - Gets the boolean value of pixel color. True if white
// Parameter: imageFile       - File containing the image
//            resolution      - Bits per pixel needed to know the colors values
// Returns:   Boolean value of pixel. True if black, false if white
//=============================================================================
static bool GetPixelValue( int resolution, std::fstream *imageFile );








//================================= Functions definitions ====================================

//=============================================================================
FileHandling::FileHandling( char *fileName )
{
  imageFile.open( fileName, std::ios::binary | std::ios::in );
  if( true == imageFile.is_open() )
  {
    std::cout << "====================================================" << std::endl;
    std::cout << "               BarCodeReader" << std::endl;
    std::cout << "Author: Bartosz Klakla\nIndex:  194573\n" << std::endl;
    std::cout << "File opened successfully\n" << std::endl;
    std::cout << "File reading status: " << ReadHeader() << std::endl;
  }
  else
  {
    std::cout << "Couldn't open the file" << std::endl;
  }

}
//=============================================================================





//=============================================================================
FileHandling::~FileHandling( )
{
}
//=============================================================================




//=============================================================================
bool FileHandling::ReadPixelRow( bool *virtualRow )
{
  // Store the padding byte-indicator
  const int paddingIndicator = 4;
  // Store the status of reading the row
  bool rowStatus = true;
  // Count the padding size for this image
  int widthInBytes = (Header.DIB.bitsPerPixel / BITS_IN_BYTE)*Header.DIB.bitmapWidth;
  // Store the number of pixels already read
  int pixelRead = Header.DIB.bitmapWidth;
  for( pixelRead = 0; pixelRead < Header.DIB.bitmapWidth; ++pixelRead )
  {
    // Read the byte after byte and convert
    virtualRow[ pixelRead ] = GetPixelValue( Header.DIB.bitsPerPixel, &imageFile );
  }

  // Check if padding is neccessary
  if( widthInBytes < paddingIndicator )
  {
    int paddingBytes = paddingIndicator - widthInBytes;
    do
    {
      paddingBytes--;
      std::cout << imageFile.get( );
    } while( paddingBytes );
  }
  else if( 0 != widthInBytes%paddingIndicator )
  {
    // Count the number of bytes needed to align
    int paddingBytes = widthInBytes%paddingIndicator;
    do
    {
      paddingBytes--;
      std::cout << imageFile.get();
    } while( paddingBytes );
  }
  
  return rowStatus;
}
//=============================================================================





//=============================================================================
bool FileHandling::ReadHeader( void )
{
  // Store the correctness of the header
  bool headerStatus = false;


  // Make sure proper PNG file was opened
  if( true == EnsureBMPFile( &imageFile ) )
  {
    // Get the size of the image
    int movingCounter = 0;
    while( movingCounter < STD_INT_SIZE )
    {
      Header.imageFileSize |= (imageFile.get( ) << (movingCounter*BITS_IN_BYTE));
      movingCounter++;
    }

    // Pass the reserved bytes in the header
    for( movingCounter = 0; movingCounter < 4; movingCounter++ )
    {
      Header.reservedBytes[ movingCounter ] = imageFile.get();
    }

    // Store the pixel array offset
    movingCounter = 0;
    while( movingCounter < STD_INT_SIZE )
    {
      Header.pixelArrayOffset |= (imageFile.get( ) << (movingCounter*BITS_IN_BYTE));
      movingCounter++;
    }
    std::cout << "#  Pixel array address is: " << Header.pixelArrayOffset << std::endl;
    // Action performed well - notify the user
    std::cout << "#  Header match!" << std::endl;
    headerStatus = FillBMPStruct();
  }

  // Return the final result
  return headerStatus;
}
//=============================================================================




//=============================================================================
bool EnsureBMPFile( std::fstream *imageFile )
{
  // store the finding
  bool findingResult = false;

  // Prepare the pattern for the comparison
  const char *compPattern[] = { "PT", "BA", "CI", "IC", "BM", "CP" };
  const int patternLength = (sizeof(compPattern) / sizeof(*compPattern));

  // Make sure file was correctly opened
  if( true == imageFile->is_open() )
  {
    // Read the header in searching for the bitmap system indication
    int headerCounter = 0;
    char imageHeaderID[ 2 ] = { imageFile->get(), imageFile->get() };
    std::cout << "#  ImageHeader is: " << imageHeaderID[ 0 ] << imageHeaderID[ 1 ] << std::endl;
    while( patternLength > headerCounter )
    {
      if( compPattern[ headerCounter ][ 0 ] == imageHeaderID[0] &&
          compPattern[ headerCounter ][ 1 ] == imageHeaderID[1]    )
      {
        // The header ID was matched.
        findingResult = true;
        break;
      }
      else
      {
        headerCounter++;
      }
    }

    // Make sure the matched header pattern is the right one
    if( imageHeaderID[ 0 ] == compPattern[ headerCounter ][ 0 ] &&
        imageHeaderID[ 1 ] == compPattern[ headerCounter ][ 1 ]    )
    {
      std::cout << "#  Bitmap ensured" << std::endl;
    }
  }

  // Notify about the finding
  return findingResult;
}
//=============================================================================





//=============================================================================
bool FileHandling::FillBMPStruct( void )
{
  // store the local error
  bool localError = true;
  const int bitPasser = GetProperInfoSize( &imageFile );

  int compressionMethod = 0;
  Header.DIB.bitmapWidth = GetValueOfInfo( bitPasser, &imageFile );
  Header.DIB.bitmapHeight = GetValueOfInfo( bitPasser, &imageFile );
  Header.DIB.planesNumber = GetValueOfInfo( STD_LOW_SIZE, &imageFile );
  Header.DIB.bitsPerPixel = GetValueOfInfo( STD_LOW_SIZE, &imageFile );

  // Get the compression method
  if( 4 == bitPasser )
  {
    int compressionMethod = GetValueOfInfo( bitPasser, &imageFile );
    if( BI_BITFIELDS == compressionMethod || BI_ALPHABITFIELDS == compressionMethod )
    {
      std::cout << "#  Important compression method spotted: " << compressionMethod << std::endl;
      localError = false;
      // Here should be gathered the extra bit masks
    }
    else if( false == compressionMethod )
    {
      std::cout << "#  Compression is: BI_RGB with status" << std::endl;
    }
    else
    {
      std::cout << "#  Unspecified compression" << std::endl;
      localError = false;
    }
    // Pass the rest of the header
    std::cout << "#  Information about the opened picture: " << std::endl;
    std::cout << "   Image size  = " << GetValueOfInfo( STD_INT_SIZE, &imageFile ) << std::endl;
    std::cout << "   Resolution  = " << GetValueOfInfo( STD_INT_SIZE, &imageFile ) << "x"
                                  << GetValueOfInfo( STD_INT_SIZE, &imageFile ) << std::endl;
    Header.DIB.colorInPalete = GetValueOfInfo( STD_INT_SIZE, &imageFile );
    std::cout << "   Colors      = " << Header.DIB.colorInPalete << std::endl;
  }


  std::cout << "   Width       = " << Header.DIB.bitmapWidth << std::endl;
  std::cout << "   Height      = " << Header.DIB.bitmapHeight << std::endl;
  std::cout << "   Plans number= " << Header.DIB.planesNumber << std::endl;
  std::cout << "   Bits/pixel  = " << Header.DIB.bitsPerPixel << std::endl;

  // Return the final result
  return localError;
}
//=============================================================================





//=============================================================================
int GetProperInfoSize( std::fstream *imageFile )
{
  // Store the final info size
  int infoSize = 0;

  // Get the size of the header
  int movingCounter = 0;
  while( movingCounter < STD_INT_SIZE )
  {
    infoSize |= (imageFile->get( ) << movingCounter);
    movingCounter++;
  }

  // Match the gathered size with proper header size
  if( 12 == infoSize )
  {
    infoSize = 2;
  }
  else if( 40 == infoSize )
  {
    infoSize = 4;
  }
  else
  {
    // Error in matching the DIB header
    std::cout << "#  Error matching the DIB size" << std::endl;
    infoSize = 0;
  }

  // Return the size
  std::cout << "#  DIB header size is: " << infoSize << std::endl;
  return infoSize;
}
//=============================================================================






//=============================================================================
int GetValueOfInfo( int bitPasser, std::fstream *imageFile )
{
  int movingCounter = 0;
  int infoValue = 0;
  while( movingCounter < bitPasser )   // Get the width of the image
  {
    infoValue |= (imageFile->get( ) << (movingCounter*BITS_IN_BYTE));
    movingCounter++;
  }
  return infoValue;
}
//=============================================================================





//=============================================================================
bool FileHandling::Virtualize( bool **virtualImage )
{
  // Store the status of virtualizing
  bool localError = false;
  int position = 0;
  // Make sure currently is the bitmap reading
  if( Header.pixelArrayOffset != imageFile.tellg() )
  {
    std::streampos filePosition = Header.pixelArrayOffset;
    std::cout << "\n#  Moving the file position to the " << filePosition << std::endl;
    imageFile.seekg( filePosition );
  }
  
  int row = Header.DIB.bitmapHeight;
 
  
  // Start the reading of each row by the bpp info
  std::cout << "#  Virtualizing the picture... " << std::endl;
  do
  {
    row--;
    localError = ReadPixelRow( virtualImage[ row ] );
  } while( row );

  return localError;
}
//=============================================================================





//=============================================================================
bool GetPixelValue( int resolution, std::fstream *imageFile )
{
  // Keep the necessary constants for the reading
  const int resolutionPattern[ 3 ] = { 1, 24, 32 };
  const unsigned char blackwhiteBorder = 100;

  // Store the color of current pixel
  bool pixelColor = false;

  if( resolutionPattern[ 1 ] == resolution )
  {
    // Three bytes with the color, get them all
    int blue  = imageFile->get();
    int green = imageFile->get();
    int red   = imageFile->get();

    std::cout << "\nBlue = " << blue << ", green = " << green << ", red = " << red << std::endl;
    if( blackwhiteBorder < blue && blackwhiteBorder < green && blackwhiteBorder < red )
    {
      // This color is the white
      pixelColor = false;
    }
    else
    {
      pixelColor = true;
    }
  }
#if 1
  else if( resolutionPattern[ 0 ] == resolution )
  {
    // Every pixel is contained in one bit - read all the eight
    int bitCount = 0; unsigned char imageByte = imageFile->get();
    for( bitCount = 0; bitCount < 8; bitCount++ )
    {
      
    }
  }
  else if( resolutionPattern[ 2 ] == resolution )
  {
    // Three bytes with the color, get them all
    int blue  = imageFile->get();
    int green = imageFile->get( );
    int red = imageFile->get( );
    int alphaDummy = imageFile->get( );

    if( blackwhiteBorder < blue && blackwhiteBorder < green && blackwhiteBorder < red )
    {
      // This color is the white
      pixelColor = false;
    }
    else
    {
      pixelColor = true;
    }
  }
#endif

  return pixelColor;
}
//=============================================================================




//=============================================================================
int FileHandling::giveSize( char sizeSign )
{
  int sizeVar = 0;

  switch( sizeSign )
  {
  case 'X':
    sizeVar = Header.DIB.bitmapWidth;
    break;
  case 'Y':
    sizeVar = Header.DIB.bitmapHeight;
    break;
  default:
    break;
  }

  return sizeVar;
}
//=============================================================================