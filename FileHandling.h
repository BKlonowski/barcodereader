//=================================================================================================
// File:    FileHandling.h
// Created: 10-12-2016, 14:36
//
// Contains the classess responsible for handling all the cases related to the image file.
//=================================================================================================
#pragma once

//===================================== Included =============================================
#include <iostream>
#include <fstream>





//============================== Constants and definitions ===================================

// Definitions of two most important compression methods
#define BI_BITFIELDS      3
#define BI_ALPHABITFIELDS 6






//================================== Types and globals =======================================

// Contains information contained in the DIB
typedef struct ImageBMPfileDIBHeader
{
  int bitmapWidth;    // 2 bytes of width
  int bitmapHeight;   // 2 bytes of height
  int planesNumber;   // 2 bytes containing number of color planes, must be 1
  int bitsPerPixel;   // 2 bytes with number of bits in one pixel
  int colorInPalete;  // Specifies the number of colors in the pallete

} DIBHeader;  // Its size must be 12 or 40 bytes


// Contains all the neccessary information about the image
typedef struct ImageBMPfileHeader
{
  int imageFileSize;        // 4 bytes containing the size
  char reservedBytes[ 4 ];
  int pixelArrayOffset;
  int DIBHeaderSize;        // Size of the DIB header given as 4 bytes
  DIBHeader DIB;            // Pointer to the one of DIB structure

} ImageHeader;





//===================================== Classess =============================================
class FileHandling
{
private:
  //=============================== Functions and methods =================================

  //=============================================================================
  // Function:  OpenFile      - opens the specified file in binary mode
  // Parameter: None
  // Returns:   Boolean status of operation
  //=============================================================================
  bool OpenFile( void );


  //=============================================================================
  // Function:  ReadPixelRow  - Reads one row of the file and puts the pixels in
  //                            the array.
  // Parameter: virtualRow    - Single row of the virtual image to be created
  // Returns:   Boolean status of operation
  //=============================================================================
  bool ReadPixelRow( bool *virtualRow );


  //=============================================================================
  // Function:  ReadHeader    - Reads the header and stores the information in struct
  // Parameter: None
  // Returns:   Boolean status of operation
  //=============================================================================
  bool ReadHeader( void );


  //=============================================================================
  // Function:  FillBMPStruct - Gets the image properties from the header
  // Parameter: None
  // Returns:   Boolean status of operation, TRUE if succedded.
  //=============================================================================
  bool FillBMPStruct( void );



  //==================================== Variables ========================================
  std::fstream imageFile;

  // Store the image header info
  ImageHeader Header;


public:
  //=============================== Functions and methods =================================


  //=============================================================================
  // Function:  FileHandling  - overloaded constructor creating the local file.
  // Parameter: fileName      - name of file given by the character chain
  // Returns:   None
  //=============================================================================
  FileHandling( char *fileName );


  //=============================================================================
  // Function:  FileHandling  - default destructor
  // Parameter: None
  // Returns:   None
  //=============================================================================
  ~FileHandling( );


  //=============================================================================
  // Function:  GiveSize      - Gives the specified size of image (in pixels)
  // Parameter: sizeSign      - Character specifying the size (X, or Y)
  // Returns:   Integer value of returned size
  //=============================================================================
  int giveSize( char sizeSign );


  //=============================================================================
  // Function:  Virtualize    - creates the final image stored as pixel color array
  // Parameter: None
  // Returns:   Boolean status of operation
  //=============================================================================
  bool Virtualize( bool **virtualImage );

  //==================================== Variables ========================================


};


